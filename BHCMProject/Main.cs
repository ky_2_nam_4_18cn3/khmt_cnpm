﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BHCMProject
{
    public partial class Main : Form
    {
        #region Poperty
        private Button _btnHome;
        private Button _btnSetting;
        private Form _activeForm;
        #endregion

        #region contructor
        public Main()
        {
            InitializeComponent();
        }
        #endregion

        private void Main_Load(object sender, EventArgs e)
        {

        }

        #region Function
        private void OpenScreen(Form childForm, object btn)
        {
            if(_activeForm != null)
            {
                _activeForm.Close();
            }
            _activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelScreen.Controls.Add(childForm);
            this.panelScreen.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void btnHome_Click(object sender, EventArgs e)
        {
            OpenScreen(new Forms.Home_Screen(), sender);
        }
        private void btnSetting_Click(object sender, EventArgs e)
        {
            OpenScreen(new Forms.Setting_Screen(), sender);
        }
        #endregion
    }
}
