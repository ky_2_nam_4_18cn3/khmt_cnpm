﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHCMProject.Infrastructure
{
    public class Users
    {
        public Guid Id { get; set; }
        public string UserCode { get; set; }
        public string Name { get; set; }
    }
}
